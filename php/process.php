<?php 
define('FROM_ENCODE', 'iso-8859-1');
$text = utf8_decode($_GET['text']);
$textUTF8 = urldecode($_GET['text']);


?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>P&aacute;gina generada din&aacute;micamente con PHP</title>
<link rel="stylesheet" type="text/css" href="../css/estilos.css" />
</head>
<body>

<h1>Codificaci&oacute;n de carateres</h1>

<h2>Baudot</h2>
<p>
Usando la tabla de Salomon y Motta, figura 1.4, p&aacute;gina 29. Case insensitive.<br/>
<?php echo getBaudot($text); ?>
</p>

<h2>Fieldata</h2>
<p>Haralambous p&aacute;gina 29. Case insensitive.<br/>
<?php echo getFieldata($text); ?>
</p>

<h2>ASCII</h2>
<p>Mackenzie p&aacute;gina 44:<br/>
<?php echo stringToASCII($text); ?>
</p>

<h2>EBCDIC</h2>
<p>Mackenzie p&aacute;gina 46<br/>
<?php echo getEBCDIC($text); ?>
</p>

<h2>CP437</h2>
<p>Haralambous p&aacute;gina 45<br/>
<?php echo getCP437($textUTF8); ?>
</p>


</body>
</html>
<?php


function stringToASCII($str){
    $arr1 = str_split($str);
	$result = "";
    for ($i = 0; $i < strlen($str); $i++) {
        if (ord($arr1[$i]) < 128) {
            $aux = decbin(ord($arr1[$i]));
        } else {
            $aux =  "n/a";
        }
        if (is_numeric($aux)) {
            $result .= '<div class="item"><div class="char">' . htmlentities($arr1[$i],  ENT_COMPAT | ENT_HTML401, FROM_ENCODE) . '</div><div class="code">' . sprintf("%07d",$aux) . '</div></div>';
        } else {
            $result .= '<div class="item"><div class="char">' . htmlentities($arr1[$i],  ENT_COMPAT | ENT_HTML401, FROM_ENCODE) . '</div><div class="code">' . $aux . '</div></div>';
        }
    }
    return $result;
}

function getBaudot($string)
{
    include_once("Baudot.php");
    $baudot = new Baudot();
    $data = $baudot->getEncodeKeyValue($string);
    $result = '';
    foreach ($data as $item) {
        //foreach ($item as $key => $value) {
            $result .= '<div class="item"><div class="char">' . htmlentities($item['char'],  ENT_COMPAT | ENT_HTML401, FROM_ENCODE) . '</div><div class="code">' . $item['code'] . '</div></div>';
        //}
    }
    return $result;
}



function getEBCDIC($string)
{
    include_once("EBCDIC.php");
    $baudot = new EBCDIC();
    $data = $baudot->getEncodeKeyValue($string);
    $result = '';
    foreach ($data as $item) {
        if (is_numeric($item['code'])) {
            $result .= '<div class="item"><div class="char">' . htmlentities($item['char'],  ENT_COMPAT | ENT_HTML401, FROM_ENCODE) . '</div><div class="code">' . sprintf('%08d', decbin($item['code'])) . '</div></div>';
        } else {
            $result .= '<div class="item"><div class="char">' . htmlentities($item['char'],  ENT_COMPAT | ENT_HTML401, FROM_ENCODE) . '</div><div class="code">' . $item['code'] . '</div></div>';
        }
    }
    return $result;
}


function getFieldata($string)
{
    include_once("Fieldata.php");
    $fieldata = new Fieldata();
    $data = $fieldata->getEncodeKeyValue($string);
    $result = '';
    foreach ($data as $item) {
        //foreach ($item as $key => $value) {
            $result .= '<div class="item"><div class="char">' . htmlentities($item['char'],  ENT_COMPAT | ENT_HTML401, FROM_ENCODE) . '</div><div class="code">' . $item['code'] . '</div></div>';
        //}
    }
    return $result;
}


function getCP437($string)
{
    include_once("CP437.php");
    $cp = new CP437();
    $data = $cp->getEncodeKeyValue($string);
    $result = '';
    foreach ($data as $item) {
        if (is_numeric($item['code'])) {
            $result .= '<div class="item"><div class="char">' . htmlentities($item['char'],  ENT_COMPAT | ENT_HTML401, 'utf-8') . '</div><div class="code">' . sprintf('%08d', decbin($item['code'])) . '</div></div>';
        } else {
            $result .= '<div class="item"><div class="char">' . htmlentities($item['char'],  ENT_COMPAT | ENT_HTML401, 'utf-8') . '</div><div class="code">' . $item['code'] . '</div></div>';
        }
    }
    return $result;
}