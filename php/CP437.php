<?php

function unichr($u) {
    return mb_convert_encoding('&#' . intval($u) . ';', 'UTF-8', 'HTML-ENTITIES');
}

class CP437
{
    var $table;
    
    function __construct() 
    {
        $c = file_get_contents(__DIR__ . '/cp437.tsv');
        //var_dump($c);
        $lines = explode("\n", $c);
        $i = 0;
        foreach ($lines as $l) {
            $line = explode("\t", $l);
            //echo $line[0] . ' '. unichr(hexdec(str_replace('0x', '', $line[1]))) . ' '. $line[2] . "\n";
            $char = unichr(hexdec(str_replace('0x', '', $line[1])));
            $value = hexdec(trim(str_replace('0x', '', $line[0])));
            if (!isset($this->table[$char])) {
                $this->table[$char] = $value;
            }
            $i++;
        }
        //var_dump($this->table);
    }


    // get the code value of a char
    //returns order starting from 0
    public function getCode($char)
    {
        if (isset($this->table[$char])) {
            return $this->table[$char];
        }
        return null;
    }
    
    
    //get CP437 encode 
    // returns 
    public function getEncodeKeyValue($string)
    {
        $encode = [];
        for ($i = 0; $i < mb_strlen($string); $i++) {
            $c = mb_substr($string, $i, 1, 'UTF-8');
            if ($this->getCode(($c)) !== null) {
                $encode[] = ['char' => ($c), 'code' => $this->getCode($c)];
            } else {
                $encode[] = ['char' => ($c), 'code' => 'n/a'];
            }
        }
        return $encode;
    }

}