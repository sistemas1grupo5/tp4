<?php

class Baudot
{
    var $mode;
    
    // from Salomon Motta, figure 1.4, page 29
    var $tableLetters = [
            'A' => '10000',
            'B' => '00110',
            'C' => '10110',
            'D' => '11110',
            'E' => '01000',
            'F' => '01110',
            'G' => '01010',
            'H' => '11010',
            'I' => '01100',
            'J' => '10010',
            'K' => '10011',
            'L' => '11011',
            'M' => '01011',
            'N' => '01111',
            'O' => '11100',
            'P' => '11111',
            'Q' => '10111',
            'R' => '00111',
            'S' => '00101',
            'T' => '10101',
            'U' => '10100',
            'V' => '11101',
            'W' => '01101',
            'X' => '01001',
            'Y' => '00100',
            'Z' => '11001',
            'LS' => '00001',
            'FS' => '00010',
            'CR' => '11000',
            'LF' => '10001',
            'ER' => '00011',
            'na' => '00000',
        ];
        
    var $tableFigures = [
            '1' => '10000',
            '8' => '00110',
            '9' => '10110',
            '0' => '11110',
            '2' => '01000',
            'na' => '01110',
            '7' => '01010',
            '+' => '11010',
            'na' => '01100',
            '6' => '10010',
            '(' => '10011',
            '=' => '11011',
            ')' => '01011',
            'na' => '01111',
            '5' => '11100',
            '%' => '11111',
            '/' => '10111',
            '-' => '00111',
            ' ' => '00101',
            'na' => '10101',
            '4' => '10100',
            "'" => '11101',
            '?' => '01101',
            ',' => '01001',
            '3' => '00100',
            ':' => '11001',
            'LS' => '00001',
            'FS' => '00010',
            'CR' => '11000',
            "\n" => '10001',
            'ER' => '00011',
            'na' => '00000',
        ];

    /*
     * get representation mode (Table)
     * letter or figure (null if not detected)
     * returns string whit the binary representation
    */
    public function getMode($aChar)
    {
        $char = strtoupper($aChar);
        if (isset($this->tableLetters[$char])) {
            return 'LS';
        }
        if (isset($this->tableFigures[$char])) {
            return 'FS';
        }
        return null;
    }
    

    // get the code value of a char
    //returns string whit the binary representation
    public function getCode($aChar)
    {
        $char = strtoupper($aChar);
        if (isset($this->tableLetters[$char])) {
            return $this->tableLetters[$char];
        }
        if (isset($this->tableFigures[$char])) {
            return $this->tableFigures[$char];
        }
        return 'n/a';
    }
    
    //get baudot encode 
    // returns 
    public function getEncode($string)
    {
        $encode = [];
        for ($i = 0; $i < strlen($string); $i++) {
            $c = substr($string, $i, 1);
            $newMode = $this->getMode($c);
            if (isset($newMode) and ($this->mode != $newMode)) {
                $this->mode = $newMode;
                $encode[] = $this->getCode($newMode);
            }
            
            $encode[] = $this->getCode($c);
        }
        return $encode;
    }
    
    //get baudot encode 
    // returns 
    public function getEncodeKeyValue($string)
    {
        $encode = [];
        for ($i = 0; $i < strlen($string); $i++) {
            $c = substr($string, $i, 1);
            $newMode = $this->getMode($c);
            if (isset($newMode) and ($this->mode != $newMode)) {
                $this->mode = $newMode;
                $encode[] = ['char' => $newMode, 'code' => $this->getCode($newMode)];
            }
            
            $encode[] = ['char' => $c, 'code' => $this->getCode($c)];
        }
        return $encode;
    }
}
